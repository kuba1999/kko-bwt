/**
 * @author Jakub Svestka - xsvest05
 * @date 17. 2. 2017
 * main.c
 *
 * Hlavni soubor. Stara se o zpracovani argumentu, otevreni souboru a nasledne zavolani funkce
 * pro kodovani ci dekodovani
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include "main.h"
#include "bwted.h"


int
main (int argc, char **argv)
{
  FILE *input = NULL;
  FILE *output = NULL;
  FILE *log = NULL;
  tErrorCode e = E_OK;
  tConfig conf;
  tBWTED bwted;

    bwted.codedSize = 0;
    bwted.uncodedSize = 0;
    conf.inpFile = NULL;
    conf.outFile = NULL;
    conf.logFile = NULL;

    if((e = parseArguments(argc, argv, &conf)) != E_OK) { //chyba
        return e;
    }

    if(conf.action == HELP) { //help message was printed
        return E_OK;
    }

    if((e = openFiles(&input, &output, &log, &conf)) != E_OK) { //chyba
        return e;
    }

    //kodovani / dekodovani
    if (conf.action == ENCODE)
    {
        e = BWTEncoding (&bwted, input, output);
    } else {
        e = BWTDecoding (&bwted, input, output);
    }

    if (log != NULL) {
        if(fprintf(
                log,
                "login = xsvest05\nuncodedSize = %" PRId64 "\ncodedSize = %" PRId64 "\n",
                bwted.uncodedSize,
                bwted.codedSize
        ) <= 0) {
          return E_WRITE;
        }
    }

    closeFiles(&input, &output, &log);

    return e;
}

/**
 * Zpracovani argumentu
 * @param argc
 * @param argv
 * @param conf
 * @return
 */
tErrorCode 
parseArguments (int argc, char **argv, tConfig *conf)
{
  uint8_t cflag = 0;
  uint8_t xflag = 0;
  uint8_t hflag = 0;
  int c;

  /*
  * source from example: https://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html
  */
  opterr = 0;
  while ((c = getopt (argc, argv, "i:o:l:cxh")) != -1)
    switch (c)
      {
      case 'c':
        cflag = 1;
        break;
      case 'x':
        xflag = 1;
        break;
      case 'h':
        hflag = 1;
      case 'i':
        conf->inpFile = optarg;
        break;
      case 'l':
        conf->logFile = optarg;
        break;
      case 'o':
        conf->outFile = optarg;
        break;
      case '?':
        if (optopt == 'i' || optopt == 'o' || optopt == 'l')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
      default: {
          fprintf(stderr, "For more info run %s -h\n", argv[0]); //otherwise print error
          return E_ARG;
        }
        
      }

      if(hflag) {
        usage (argv[0]);
        conf->action = HELP;
        return E_OK;
      }

      if(cflag && !xflag) { //encoding
        conf->action = ENCODE;
      }
      else if(!cflag && xflag) { //decoding
        conf->action = DECODE;
      }
      else if(!cflag && !xflag) { //nothing
        fprintf(stderr, "Either -c or -x must be set\n");
        fprintf(stderr, "For more info run %s -h\n", argv[0]);
        return E_ARG;
      }
      else { //nothing
        fprintf(stderr, "Parameters -c and -x are mutually exclusive\n");
        fprintf(stderr, "For more info run %s -h\n", argv[0]);
        return E_ARG;
      }


  return E_OK;
}

/**
 * Otevreni potrebych souboru
 * @param input
 * @param output
 * @param log
 * @param conf
 * @return
 */
tErrorCode 
openFiles (FILE **input, FILE **output, FILE **log, tConfig *conf)
{
  tErrorCode e;

  //input file
  if(conf->inpFile != NULL) {
    e = safeOpenFile (input, conf->inpFile, "rb");

    if(e != E_OK) {
      fprintf(stderr, "Unable to open input file: %s\n", conf->inpFile);
      return e;
    }
  } else {
    *input = stdin;
  }

  //output file
  if(conf->outFile != NULL) {
    e = safeOpenFile (output, conf->outFile, "wb");

    if(e != E_OK) {
      fprintf(stderr, "Unable to open output file: %s\n", conf->outFile);
      return e;
    }
  } else {
    *output = stdout;
  }

  //log file
  if(conf->logFile != NULL) {
    e = safeOpenFile (log, conf->logFile, "w");

    if(e != E_OK) {
      fprintf(stderr, "Unable to open log file: %s\n", conf->logFile);
      return e;
    }
  }

  return E_OK;
}

/**
 * Otevreni souboru s osetrenim chyby
 * @param file
 * @param path
 * @param mode
 * @return
 */
tErrorCode
safeOpenFile(FILE **file, const char *path, const char *mode)
{
  *file = NULL;

  if((*file = fopen(path, mode)) == NULL) {
    return E_FILE;
  }

  return E_OK;
}

/**
 * Zavreni otevrenych souboru
 * @param input
 * @param output
 * @param log
 */
void 
closeFiles (FILE **input, FILE **output, FILE **log)
{
  if(*input != NULL) {
    fclose(*input);
  }
  if(*output != NULL) {
    fclose(*output);
  }
  if(*log != NULL) {
    fclose(*log);
  }
}

/**
 * Zprava s napovedou
 * @param fileName
 */
void 
usage(const char *fileName)
{
  const char *help = 
    "KKO - Burrows-Wheeler transformation | xsvest05 | VUT FIT\n"
    "---------------------------------------------------------\n\n"
    "usage:\n";

  const char* help2 = 
    " [options] [action]\n\n" 
    "options:\n"
    "  -i <ifile>     input file, default: <stdin>\n"
    "  -o <ofile>     output file, default: <stdout>\n"
    "  -l <lfile>     log file\n"
    "action:\n"
    "  -c             encoding file\n"
    "  -x             decoding file\n"
    "  (parameters -c and -x are mutually exclusive)\n"
    "other:\n"
    "  -h             displays this help message\n";


    fprintf(stderr, "%s", help);
    fprintf(stderr, "  %s", fileName);
    fprintf(stderr, "%s\n", help2);
}


