CC=gcc
PROJ=bwted
CFLAGS=-std=c11 -Wall -Wextra -D_GNU_SOURCE -Wp,-D_FORTIFY_SOURCE=2 -Wwrite-strings -Winline -Wshadow -Wall -Wextra -lm
OPTFLAGS=-O2 -g
LDFLAGS=
SOURCES=main.c bwted.c shcAlgorithm.c
HEADERS=main.h bwted.h shcAlgorithm.h
ARCHIVE=kko.proj2.xsvest05

$(PROJ): $(SOURCES) $(HEADERS)
	$(CC) $(SOURCES) $(CFLAGS) $(OPTFLAGS) $(LDFLAGS) -o $@

clean:
	rm -rf $(PROJ)
	rm -rf *.o
	rm -rf $(ARCHIVE).zip

archive:
	mkdir $(ARCHIVE)
	cp $(SOURCES) $(ARCHIVE)
	cp $(HEADERS) $(ARCHIVE)
	cp Makefile $(ARCHIVE)
	rm -rf $(ARCHIVE).zip
	zip -r $(ARCHIVE).zip $(ARCHIVE)
	rm -rf $(ARCHIVE)
