/**
 * @author Jakub Svestka - xsvest05
 * @date 17. 2. 2017
 * shcAlgorithm.c
 *
 * Funkce pro vytvoreni a praci se stromem dle algoritmu SHC pro vytvoreni Huffmanova kodu
 */

#include "shcAlgorithm.h"

/**
 * Inicializace stromu
 * @param tree
 * @return
 */
tErrorCode
initSHC(tTree **tree)
{
    //alokovani struktury pro strom
    *tree = calloc(1, sizeof(tTree));
    if( *tree == NULL ) {
        return E_MALLOC;
    }

    //alokovani ukazatelu na uzly
    (*tree)->nodes = calloc(MAX_NODE_COUNT, sizeof(tNode *));
    if( (*tree)->nodes == NULL ) {
        return E_MALLOC;
    }

    //pomocne pole pro vyhlednai uzlu dle ASCII hodnoty
    (*tree)->leafs = calloc(MAX_LEAF_COUNT, sizeof(tNode *));
    if( (*tree)->leafs == NULL ) {
        return E_MALLOC;
    }

    (*tree)->size = MAX_NODE_COUNT;
    (*tree)->nextPosition = 0;
    (*tree)->root = NULL;

    //nastaveni na NULL
    for (size_t i = 0; i < MAX_NODE_COUNT; i++) {
        (*tree)->nodes[i] = NULL;
    }
    for (size_t i = 0; i < MAX_LEAF_COUNT; i++) {
        (*tree)->leafs[i] = NULL;
    }

    return E_OK;
}

/**
 * Zruseni stromu => uvolneni zdroju
 * @param tree
 */
void
disposeSHC(tTree **tree)
{
    for (size_t i = 0; i < (*tree)->size; i++) {
        if((*tree)->nodes[i] != NULL) {
            free((*tree)->nodes[i]);
        }
    }

    free((*tree)->leafs);
    free((*tree)->nodes);
    free(*tree);
}

/**
 * Vytvoreni listu
 * @param tree
 * @param value
 * @return
 */
tNode *
createLeafSHC(tTree **tree, uint8_t value)
{
    tNode *node = insertNodeSHC(tree, value, 1, NULL, NULL, NULL);

    (*tree)->leafs[value] = node; //ulozeni listu do pole listu pod klicem = ASCII hodnota

    return node;
}

/**
 * Vytvoreni rodicovskeho uzlu
 * @param tree
 * @param freq
 * @param left
 * @param right
 * @return
 */
tNode *
createParentNodeSHC(
        tTree **tree,
        size_t freq,
        struct sNode *left,
        struct sNode *right
) {
    tNode *parent = insertNodeSHC(tree, '-', freq, left, right, NULL);

    left->parent = parent; //nastaveni rodice u leveho uzlu
    left->son = LEFT; //typ syna

    right->parent = parent; //nastaveni rodice u praveho uzlu
    right->son = RIGHT; //typ syna

    return parent;
}

/**
 * Vlozeni obecneho uzlu - vyuzitou v predchozich dvou funkcich
 * @param tree
 * @param value
 * @param freq
 * @param left
 * @param right
 * @param parent
 * @return
 */
tNode *
insertNodeSHC(
        tTree **tree,
        uint8_t value,
        size_t freq,
        struct sNode *left,
        struct sNode *right,
        struct sNode *parent
) {
    tNode *node = calloc(1, sizeof(tNode));

    //ulozime do stromu
    (*tree)->nodes[ (*tree)->nextPosition++ ] = node;

    node->value = value;
    node->freq = freq;
    node->parent = parent;
    node->left = left;
    node->right = right;
    node->son = NONE;
    node->codeLength = 0;

    //vynulovani kodu
    memset (node->code, 0, 16);

    return node;
}

/**
 * Vygenerovani kodu v listovych uzlech
 * @param node
 */
void
generateCodeForLeaf(tNode *node)
{
    //nastane v pripade, pokud mame jen jeden uzel, ktery je zaroven korenem
    if (node->parent == NULL) {
        node->code[0] = '0';
        node->codeLength++;
        return;
    }

    //od listoveho uzlu postupujeme ke koreni a doplnujeme kod
    tNode *tmpNode = node;
    unsigned char pos = 0;
    while (tmpNode != NULL) {
        if (tmpNode->parent != NULL) {
            node->code[pos++] = (uint8_t)('0' + tmpNode->son); //doplneni 1 bitu kodu
            node->codeLength++;
        }
        tmpNode = tmpNode->parent; //pokracujeme dale rodicem
    }

    if ( node->codeLength == 0 ) {
        return;
    }

    //reverzace kodu
    char temp;
    size_t j = node->codeLength - 1;   // posledni index
    size_t i = 0;       // prvni index
    while (i < j) {
        temp = node->code[i];
        node->code[i] = node->code[j];
        node->code[j] = temp;
        i++;
        j--;
    }
}

/**
 * Zmena velikosti alokovaneho zdroje pro uzly na zaklade postu listu
 * @param tree
 */
void
setRealSizeSHC(tTree **tree)
{
    size_t nodesCount = 2 * ((*tree)->nextPosition) - 1;  //(*tree)->nextPosition - 1 + 1 = (*tree)->nextPosition = pocet symbolu

    (*tree)->nodes = realloc((*tree)->nodes, nodesCount * sizeof(tNode *));
    (*tree)->size = nodesCount;
}

/*
void
printTree(tTree **tree)
{
    printf("ID\t\tRODIC\t\tsynL\t\tsynR\t\tsyn\t\tHodnota\tFrekvence\tKod\n");
    for (size_t i = 0; i < (*tree)->size; i++) {
        if((*tree)->nodes[i] != NULL)
        {
            printf("%9d\t%13d\t%13d\t%13d\t%3c\t%15c (%d)\t%9d\t%.*s\n",
                   ((void*)(*tree)->nodes[i]),
                   (*tree)->nodes[i]->parent==NULL ? -1 : ((void*)(*tree)->nodes[i]->parent),
                   (*tree)->nodes[i]->left==NULL ? -1 : ((void*)(*tree)->nodes[i]->left),
                   (*tree)->nodes[i]->right==NULL ? -1 : ((void*)(*tree)->nodes[i]->right),
                   (*tree)->nodes[i]->son==LEFT ? 'L' : ((*tree)->nodes[i]->son==RIGHT ? 'P' : '-'),
                   (int)(*tree)->nodes[i]->value,
                   (int)(*tree)->nodes[i]->value,
                   (int)(*tree)->nodes[i]->freq,
                   (*tree)->nodes[i]->codeLength,
                   (*tree)->nodes[i]->code
            );
        }
    }

    if( (*tree)->root != NULL ) {
        printf("root: %d\n", ((void*)(*tree)->root));
    }
}*/

/**
 * Serazeni uzlu
 * @param tree
 */
void
sortNodes(tTree **tree)
{
    /**
    * Funkce pro lexikograficke serazeni slupce L
    */
    int compare (const void * _a, const void * _b)
    {
        const tNode **a = (const tNode **) _a;
        const tNode **b = (const tNode **) _b;

        //pokud uzel ma rodice, posuneme ho vpravo
        if( (**a).parent != NULL ) {
            return 1;
        }

        //pokud uzel ma rodice, posuneme ho vpravo
        if( (**b).parent != NULL ) {
            return -1;
        }

        //pokud je frekvence vyskytu shodna, tak radime dle hodnoty
        if( (**a).freq == (**b).freq ) {
            return (**a).value - (**b).value;
        }

        //jinak radime dle frekvence
        return (**a).freq - (**b).freq;
    }

    //serazeni, radime jen druhy sloupec (F)
    qsort ((*tree)->nodes, (*tree)->nextPosition, sizeof(tNode *), compare);
}