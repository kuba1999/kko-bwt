#!/bin/sh

if [ "$1" = '' ] || [ "$1" = '-h' ] || [ "$1" = '--help' ]; then
  cat <<EOF

Test script for VUT FIT KKO proj2 - bwted (Burrows-Wheeler transform)
Author: Filip Moc <xmocfi00@stud.fit.vutbr.cz>

recommended usage:
  - make empty directory and enter it
  - optionally copy this script and/or your zip archive to this directory
  - run this script with environment variable USER set to your login and argument pointing to your zip archive
  (you don't need to set USER env on merlin or eva)
  - on bsd you may need to set env MAKE='gmake'
  - when finished, script will print results summary

example 1:
  $ mkdir test
  $ cd test
  $ USER=xlogin00 nice -n 19 ../kko.proj2.test.sh ../kko.proj2.xlogin00.zip

example 2:
  $ mkdir test
  $ cd test
  $ cp -i ../kko.proj2.test.sh ../kko.proj2.xlogin00.zip ./
  $ chmod a+x kko.proj2.test.sh
  $ USER=xlogin00 nice -n 19 ./kko.proj2.test.sh kko.proj2.xlogin00.zip


notes:
  - on first run script will generate directory 'testset'
  - you may delete this directory to make script regenrate
    new testset
  - directories 'testset' and 'tmp' are expected to become
    big (~500MiB) so don't forget to delete them to free
    disk space when you're finished testing
  - required API (e.g. bwted.h) is not tested in any way

what it does test:
  - archive name
  - required files
  - compilation
  - help message
  - various commands with weird arguments
  - files (-i/-o)
  - nonexistent files
  - writing to /dev/full
  - crash (e.g. SIGSEGV) detected even when failure is expected
  - compression and decompression with binary match check
    of original and compression cycled file using 'cmp'
  - logfile (-l) using 'diff'
  - compression ratios
  - optional rerun with valgrind enabled

EOF
  exit;
fi

checkfile()
{
  printf '  checking for %s\n' "$1" >&2;
  if ! [ -s "$DIR/$1" ]; then
    printf '    ERROR: file %s is missing!\n' "$DIR/$1" >&2;
    return 1;
  fi
  true;
}

unpack() {
  bname="`basename "$1"`";
  bname_ref="kko.proj2.$USER.zip";
  if [ "$bname" != "$bname_ref" ]; then
    printf 'bad archive name %s, expected %s\n' "$bname" "$bname_ref" >&2;
    exit 1;
  fi
  printf 'unpacking archive %s\n' "$bname" >&2;
  unzip "$1";
}

formal() {
  printf 'running formal tests\n' >&2;
  bad='';
  checkfile 'bwted.h' || bad=1;
  checkfile 'bwted.c' || bad=1;
  checkfile 'main.c' || bad=1;
  checkfile 'Makefile' || bad=1;
  checkfile 'bwted.pdf' || bad=1;
  printf 'formal tests finished\n' >&2;
  ! [ "$bad" ];
}

runtest() {
  mkdir -p 'log';
  log="log/$1.log";
  if [ "$2" ]; then
    mkdir -p "log/$1";
    log="log/$1/$2.log";
  fi
  ("$@"; printf '%i' "$?" > "$log.exit") 2>&1 | tee "$log";
  MSG='OK';
  if [ "`cat "$log.exit"`" != '0' ]; then
    if [ "`cat "$log.exit"`" = '2' ]; then
      MSG="WARNING, see $log";
    else
      MSG="ERROR, see $log";
    fi
  fi
  name="$@";
  g_summary="$g_summary`printf '%24s' "$name"`: $MSG
";
}

compile()
{
  printf 'compiling using make\n' >&2;
  (
    cd "$DIR" || exit 1;
    $MAKE;
  ) || exit 1;
  printf 'compilation finished\n' >&2;
}

BWTED() {
  $VALGRIND "$DIR/bwted" "$@";
}

helptests() {
  printf 'running help tests\n' >&2;
  bad='';
  for c in '-c' '-x' '-i' '-o' '-l'; do
    printf '  checking for %s\n' "$c" >&2;
    BWTED -h 2>&1 | fgrep -q -- "$c";
    if [ "$?" != '0' ]; then
      printf '    ERROR: option %s is missing in help\n' "$c" >&2;
      bad=1;
    fi
  done
  ! [ "$bad" ];
}

gentests() {
  mkdir -p 'testset/data' 'testset/ratio';
  printf 'generating tests\n' >&2;

  printf '  gen random mini\n' >&2;
  head -c 123 /dev/urandom > 'testset/data/minirandom';
  printf '200' > "testset/ratio/minirandom";

  size='2097152';
  for i in `seq -w 1 16`; do
    printf '  gen random big %s/16\n' "$i" >&2;
    head -c "$size" /dev/urandom > "testset/data/zbigrandom$i";
    size="`head -c 3 /dev/urandom |  hexdump -e '"%d"'`";
    printf '200' > "testset/ratio/zbigrandom$i";
  done

  for p in man ls cp mv strlen memcpy bash; do
    printf '  gen man page %s\n' "$p" >&2;
    man "$p" > "testset/data/man_$p";
    printf '75' > "testset/ratio/man_$p";
  done

  for b in ls cp mv sh ln; do
    printf '  gen bin %s\n' "$b" >&2;
    cat "/bin/$b" > "testset/data/bin_$b";
    printf '90' > "testset/ratio/bin_$b";
  done
}

ectest() {
  minec="$1";
  maxec="$2";
  shift;
  shift;
  printf '  exit test:  %s   -- in range %i %i\n' "$1" "$minec" "$maxec" >&2;
  eval "$1" >/dev/null 2>'tmp/stderr';
  ec="$?";
  if [ "$ec" -lt "$minec" ] || [ "$ec" -gt "$maxec" ]; then
    printf '    ERROR: exited with code: %s (out of range %i - %i)\n' "$ec" "$minec" "$maxec" >&2;
    cat 'tmp/stderr' >&2;
    return 1;
  fi
  true;
}

argstests() {
  printf 'running arguments tests\n' >&2;
  bad='';

  ectest 0 127 'BWTED -h' || bad=1;
  ectest 0 0 'true | BWTED -c' || bad=1;
  ectest 0 0 'cat "testset/data/minirandom" | BWTED -c' || bad=1;
  ectest 0 0 'true | BWTED -c | BWTED -x' || bad=1;
  ectest 0 0 'true | BWTED -c | BWTED -x' || bad=1;

  ectest 0 127 'true | BWTED -x' || bad=1;
  ectest 0 127 'cat "testset/data/minirandom" | BWTED -x' || bad=1;
  ectest 0 127 'cat "testset/data/minirandom" | BWTED -c -x' || bad=1;
  ectest 0 127 'cat "testset/data/minirandom" | BWTED -x -c' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -o' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -i' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -l' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -c -o' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -c -i' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -c -l' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -x -o' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -x -i' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -x -l' || bad=1;
  ectest 0 127 'cat "testset/data/minirandom" | BWTED -c -h' || bad=1;
  ectest 0 127 'cat "testset/data/minirandom" | BWTED -x -h' || bad=1;
  ectest 0 127 'cat "testset/data/minirandom" | BWTED -c -x -h' || bad=1;

  printf 'arguments tests finished\n' >&2;
  ! [ "$bad" ];
}

filestests() {
  printf 'running tests for files (-i/-o)\n' >&2;
  bad='';

  ectest 0 0 'true | BWTED -c -i /dev/stdin' || bad=1;
  ectest 0 0 'true | BWTED -c -i /dev/stdin -o /dev/stdout | BWTED -x -i /dev/stdin -o /dev/stdout' || bad=1;
  ectest 0 0 '[ "`true | BWTED -c -i /dev/stdin -o /dev/stdout | BWTED -x -i /dev/stdin -o /dev/stdout`" = "" ]' || bad=1;
  ectest 0 0 'cat "testset/data/minirandom" | BWTED -c -i /dev/stdin -o /dev/stdout' || bad=1;
  ectest 0 0 'cat "testset/data/minirandom" | BWTED -c -i /dev/stdin -o /dev/stdout | BWTED -x -i /dev/stdin -o /dev/stdout' || bad=1;
  ectest 0 0 'cat "testset/data/minirandom" | BWTED -c -i /dev/stdin -o /dev/stdout | BWTED -x -i /dev/stdin -o /dev/stdout | cmp - "testset/data/minirandom"' || bad=1;

  printf 'tests for files finished\n' >&2;
  ! [ "$bad" ];
}

nonexistent() {
  printf 'running tests for nonexistent files\n' >&2;
  bad='';

  ectest 1 127 'cat "testset/data/minirandom" | BWTED -c -o /nonexistent/foo' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -c -i /nonexistent/foo' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -x -o /nonexistent/foo' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -x -i /nonexistent/foo' || bad=1;

  printf 'tests for nonexistent files finished\n' >&2;
  ! [ "$bad" ];
}

devfull() {
  printf 'running tests for /dev/full\n' >&2;
  bad='';

  ectest 1 127 'printf x | BWTED -c -o /dev/full' || bad=1;
  ectest 1 127 'printf x | BWTED -c > /dev/full' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -c -o /dev/full' || bad=1;
  ectest 1 127 'cat "testset/data/minirandom" | BWTED -c > /dev/full' || bad=1;
  ectest 1 127 'cat "testset/data/zbigrandom01" | BWTED -c -o /dev/full' || bad=1;
  ectest 1 127 'cat "testset/data/zbigrandom01" | BWTED -c > /dev/full' || bad=1;
  ectest 1 127 'true | BWTED -c -l /dev/full' || bad=1;
  

  printf 'tests for /dev/full finished\n' >&2;
  ! [ "$bad" ];
}

cdtest() {
  printf 'running compression & decompression test for %s\n' "$1" >&2;
  cat "testset/data/$1" | BWTED -c -l "tmp/$1.bwtedlog" > "tmp/$1.bwted" || return 1;
  cat "tmp/$1.bwted" | BWTED -x > "tmp/$1.debwted" || return 1;
  cmp "tmp/$1.debwted" "testset/data/$1" || return 1;
  origsize="`wc -c "testset/data/$1" | sed -E 's/ *([^ ]*) *.*/\1/'`";
  compsize="`wc -c "tmp/$1.bwted" | sed -E 's/ *([^ ]*) *.*/\1/'`";
  printf 'login = %s\nuncodedSize = %i\ncodedSize = %i\n' "$USER" "$origsize" "$compsize" > "tmp/$1.bwtedlog.ref";
  diff "tmp/$1.bwtedlog" "tmp/$1.bwtedlog.ref" || return 1;
  maxratio="`cat "testset/ratio/$1"`";
  ratio="`printf '%i * 100 / %i\n' "$compsize" "$origsize" | bc`";
  printf '%s%%\n' "$ratio" > "tmp/$1.ratio";
  if [ "$ratio" -gt "$maxratio" ]; then
    printf '  WARNING: ratio %i%% is too big, expected at most %i%%\n' "$ratio" "$maxratio";
    return 2;
  fi
  true;
}

alltests() {
  g_summary='';

  runtest formal;

  compile || exit 1;

  runtest helptests;

  if ! [ -e "testset" ]; then
    gentests || exit 1;
  fi

  runtest argstests;
  runtest filestests;
  runtest nonexistent;
  runtest devfull;
  for t in testset/data/*; do
    t="`basename "$t"`";
    runtest cdtest "$t";
  done

  printf '\nratios:\n' >&2;
  for t in tmp/*.ratio; do
    b="`basename "$t"`";
    printf '        %s: %s\n' "$b" "`cat "$t"`";
  done
  printf '\nsummary:\n%s\n\n' "$g_summary" >&2;
}

! [ "$MAKE" ] && MAKE='make';
DIR="kko.proj2.$USER";
VALGRIND='';
if [ -e "$DIR" ]; then
  printf 'directory %s already exists, remove? (y/n) ' "$DIR" >&2;
  read a;
  [ "$a" = 'y' ] && rm -rf "$DIR";
fi
if ! [ -e "$DIR" ]; then
  unpack "$1" || exit 1;
fi
mkdir -p 'tmp';


alltests;

printf 'run all tests again with valgrind enabled? (y/n) ' >&2;
read a;
if [ "$a" = 'y' ]; then
  VALGRIND='valgrind -q --error-exitcode=255';
  printf 'check also for memory leaks (not just errors)? (y/n) ' >&2;
  read a;
  [ "$a" = 'y' ] && VALGRIND="$VALGRIND --leak-check=full";
  alltests;
fi

