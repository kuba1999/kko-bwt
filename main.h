/**
 * @author Jakub Svestka - xsvest05
 * @date 17. 2. 2017
 * main.h
 *
 * Obsahuje pomocne datove typy
 */

#ifndef MAIN_H
#define MAIN_H

#define BLOCK_SIZE BUFSIZ

//chybove kody
typedef enum {
	E_OK = 0,
	E_ARG = 1,
	E_FILE = 1,
	E_MALLOC = 1,
    E_NOT_VALID_INPUT_FILE = 1,
	E_WRITE,
} tErrorCode;

//akce pro vykonani
typedef enum {
	ENCODE = 0,
	DECODE,
	HELP,
} tProgramAction;

//struktura pro uchovani potrebnych souboru
typedef struct {
	char* inpFile;
	char* outFile;
	char* logFile;
	tProgramAction action;
} tConfig;


tErrorCode parseArguments(int, char **, tConfig *);
tErrorCode openFiles (FILE **, FILE **, FILE **, tConfig *);
void closeFiles (FILE **, FILE **, FILE **);
tErrorCode safeOpenFile(FILE **, const char *, const char *);
void usage(const char *);

#endif