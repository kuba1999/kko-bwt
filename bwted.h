/**
 * @author Jakub Svestka - xsvest05
 * @date 17. 2. 2017
 * bwted.h
 *
 * Obsahuje pomocnou strukturu pro zaznamenavani velikosti pred a po kompresi
 */

#ifndef BWTED_H
#define BWTED_H

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include "main.h"

typedef struct
{
  int64_t uncodedSize;
  int64_t codedSize;
} tBWTED;

extern tErrorCode BWTEncoding (tBWTED *, FILE *, FILE *);
extern tErrorCode BWTDecoding (tBWTED *, FILE *, FILE *);
extern tErrorCode BWTLog (tBWTED *, FILE *);

#endif