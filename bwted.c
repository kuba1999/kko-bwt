/**
 * @author Jakub Svestka - xsvest05
 * @date 17. 2. 2017
 * bwted.c
 *
 * Obsahuje funkce pro kodovani a dekodavani pomoci algoritmu
 * BWT, RLE, MTF, SHC
 */

#include "bwted.h"
#include "shcAlgorithm.h"

#define RLE_FLAG 0xFF


/**
 * BWT encode
 * @param block vstupni data
 * @param length delka vstupnich dat
 * @param encoded kodovana data
 * @param newLength delka kodovanych dat
 * @return
 */
tErrorCode
BWTEncode(const uint8_t *block, size_t length, uint8_t **encoded, size_t *newLength)
{
    //vytvreni pole suffixu ukazatelu na pocatecni znak retece
    const uint8_t **suffixArray = calloc(length, sizeof(uint8_t *));
    if(suffixArray == NULL) {
        return E_MALLOC;
    }

    //jednotlive rotace, pouze se pamatuje ukazatel na zacatek dane rotace
    for (size_t i = 0; i < length; i++) {
        suffixArray[i] = &(block[i]);
    }

    /**
     * Funkce pro quicksort, ktera zaridi serazeni vsech rotaci dle prvniho znaku (sloupce)
     */
    int compare (const void * _a, const void * _b)
    {
        const uint8_t **a = (const uint8_t **) _a;
        const uint8_t **b = (const uint8_t **) _b;

        for (size_t i = 0; i < length; i++) {
            if ( block[((*a - block + i) % length)] == block[((*b - block + i) % length)])
                continue;

            return block[((*a - block + i) % length)] - block[((*b - block + i) % length)];
        }

        return 0;
    }

    //serazeni
    qsort (suffixArray, length, sizeof(uint8_t *), compare);

    //index na rotaci odpovidajici puvodnimu textu
    uint16_t INDEX;
    //vyhledani rotace dle pocateni adresy v pameti
    for (INDEX = 0; &(suffixArray[INDEX][0]) != &(block[0]); INDEX++);

    //pole pro zakodovane data
    *encoded = (uint8_t *) calloc (length + sizeof (uint16_t), sizeof (uint8_t));
    if(*encoded == NULL) {
        return E_MALLOC;
    }

    //zakodovani bloku dat
    for (size_t i = 0; i < length; i++) {
        size_t index = (size_t) ((((void*)&suffixArray[i][0])) - ((void*)&block[0]));
        size_t lastIndex = ((length - 1) + index) % length;

        (*encoded)[i] = block[lastIndex];
    }


    //ulozeni indexu za zakodovane data
    #if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
    //MSB on left
    (*encoded)[length] = (INDEX >> 8) & 0xFF;
    (*encoded)[length + 1] = INDEX & 0xFF;
    #else
    //MSB on right
    encoded[length] = INDEX & 0xFF;
    encoded[length + 1] = (INDEX >> 8) & 0xFF;
    #endif

    *newLength = length;
    *newLength += sizeof (uint16_t);

    free(suffixArray);

    return E_OK;
}

/**
 * BWT decode
 * @param block vstupni data
 * @param length delka vstupnich dat
 * @param decoded dekodovana data
 * @param newLength delka dekodovanych dat
 * @return
 */
tErrorCode
BWTDecode(const uint8_t *block, size_t length, uint8_t **decoded, size_t *newLength)
{
    //extrakce indexu
    uint16_t INDEX = (block[length - sizeof (uint16_t)] << 8) + block[length - sizeof (uint8_t)];

    //delka zakodovaneho bloku dat
    size_t lengthWithoutIndex = length - sizeof(uint16_t);

    const uint8_t **LF = calloc(lengthWithoutIndex * 2, sizeof(uint8_t *));
    if(LF == NULL) {
        return E_MALLOC;
    }

    //vytvoreni sloupce F a L
    for (size_t i = 0; i < lengthWithoutIndex; i++) {
        LF[i] = &(block[i]);
        LF[i + lengthWithoutIndex] = &(block[i]);
    }

    /**
     * Funkce pro lexikograficke serazeni slupce L
     */
    int compare (const void * _a, const void * _b)
    {
        const uint8_t **a = (const uint8_t **) _a;
        const uint8_t **b = (const uint8_t **) _b;

        return **a - **b;
    }

    //serazeni, radime jen druhy sloupec (F)
    qsort (LF + lengthWithoutIndex, lengthWithoutIndex, sizeof(uint8_t *), compare);

    *decoded = calloc(lengthWithoutIndex, sizeof(uint8_t *));
    if(*decoded == NULL) {
        return E_MALLOC;
    }

    //samotne dekodovani
    size_t j = 0;
    size_t i = INDEX; //zaciname na radku puvodniho INDEXu
    for ( ; j < lengthWithoutIndex; j++) {
        (*decoded)[j] = *LF[ lengthWithoutIndex + (i % lengthWithoutIndex) ]; //tomu odpovida dekodovana hodnota ve sloupci F

        //zjisteni poradi dekodovaneho symbolu ve sloupci F
        int o = 1;
        //prochazime znaky vyse a zkoumame, zda se lisi od predchoziho
        //na zaklade toho jsme schopni zjistit poradi
        while (*LF[ lengthWithoutIndex + (i % lengthWithoutIndex) - o ] == (*decoded)[j] &&
               (lengthWithoutIndex + (i % lengthWithoutIndex)) - o >= lengthWithoutIndex) { //hledame jen ve sloupci F

            o++;
        }

        //vyhledame dekodovanou hodnotu v sloupci L, ktera je o-ta v poradi
        for(i = 0; i < lengthWithoutIndex ; i++) {
            if (*LF[ i ] == (*decoded)[j]) {
                if(--o == 0) {
                    break;
                }
            }
        }
    }

    //delka dekodovaneho bloku dat
    *newLength = lengthWithoutIndex;

    free(LF);

    return E_OK;
}

/**
 * MTF encode
 * @param block vstupni data
 * @param length delka vstupnich dat
 * @param encoded kodovana data
 * @param newLength delka kodovanych dat
 * @return
 */
tErrorCode
MTFEncode(const uint8_t *block, size_t length, uint8_t **encoded, size_t *newLength)
{
    uint8_t *alphabet = (uint8_t *) calloc(UINT8_MAX + 1, sizeof(uint8_t)); //+1 kvuli indexovani od nuly
    if(alphabet == NULL) {
        return E_MALLOC;
    }

    //vytvoreni abecedy
    for (size_t c = 0; c < (UINT8_MAX + 1); c++) {
        alphabet[c] = (uint8_t) c;
    }

    //zjisteni indexu pro hledany znak
    uint8_t getIndexOfChar(const uint8_t *searched) {
        for (size_t i = 0; i < (UINT8_MAX + 1); i++) {
            if(*searched == alphabet[i]) {
                return (uint8_t)i;
            }
        }

        return 0; //nikdy by se nemelo dojit sem
    }

    *encoded = (uint8_t *) calloc(length, sizeof(uint8_t *));
    if(*encoded == NULL) {
        free(alphabet);
        return E_MALLOC;
    }

    uint8_t tempValue, index;
    //pro kazdy symbol v bloku dat proved
    for (size_t i = 0; i < length; i++) {
        //vystupem bude index znaku v abecede
        index = getIndexOfChar(&(block[i]));
        (*encoded)[i] = index;

        //presunuti aktualniho znaku abecedy na zacatek abecedy
        tempValue = alphabet[index];
        for (size_t j = index; j > 0; --j) {
            alphabet[j] = alphabet[j - 1];
        }
        alphabet[0] = tempValue;
    }

    *newLength = length;
    free(alphabet);

    return E_OK;
}

/**
 * MTF decode
 * @param block vstupni data
 * @param length delka vstupnich dat
 * @param decoded dekodovana data
 * @param newLength delka dekodovanych dat
 * @return
 */
tErrorCode
MTFDecode(const uint8_t *block, size_t length, uint8_t **decoded, size_t *newLength)
{
    uint8_t *alphabet = (uint8_t *) calloc(UINT8_MAX + 1, sizeof(uint8_t)); //+1 kvuli indexovani od nuly
    if(alphabet == NULL) {
        return E_MALLOC;
    }

    //vytvoreni abecedy
    for (size_t c = 0; c < (UINT8_MAX + 1); c++) {
        alphabet[c] = (uint8_t) c;
    }

    *decoded = (uint8_t *) calloc(length, sizeof(uint8_t));
    if(*decoded == NULL) {
        free(alphabet);
        return E_MALLOC;
    }

    uint8_t tempValue;
    //pro kazdy symbol v bloku dat proved
    for (size_t i = 0; i < length; i++) {
        (*decoded)[i] = alphabet[ block[i] ];
        //printf("mtf: '%c'\n", (*decoded)[i]);

        //presunuti aktualniho znaku abecedy na zacatek abecedy
        tempValue = alphabet[ block[i] ];
        for (size_t j = block[i]; j > 0; --j) {
            alphabet[j] = alphabet[j - 1];
        }
        alphabet[0] = tempValue;
    }

    *newLength = length;
    free(alphabet);

    return E_OK;
}

/**
 * RLE encode
 * @param block vstupni data
 * @param length delka vstupnich dat
 * @param encoded kodovana data
 * @param newLength delka kodovanych dat
 * @return
 */
tErrorCode
RLEEncode(const uint8_t *block, size_t length, uint8_t **encoded, size_t *newLength)
{
    /*
     * BLOCK_SIZE * 2 je nejhorsi pripad, ktery nastane pokud vstupni data obsahuji same 0xFF
     * 0xFF je totiz rezervovany znak pro RLE, takze kazdy takovy znak nahradime dvojici 0xFF 0xFF
     */
    *encoded = (uint8_t *) calloc (BLOCK_SIZE * 2, sizeof (uint8_t));
    if(*encoded == NULL) {
        return E_MALLOC;
    }

    size_t count = 1;
    uint8_t ch = block[0];
    size_t position = 0;
    for (size_t i = 1; i <= length; i++) {
        if (i == length || block[i] != ch) { //nutno dodrzet poradi podminek, jinak budeme sahat mimo alokovanou pamet
            if( count < 4) { //pocet opakujicich se znaku je mensi jak 4 => nema cenu zakodovat (nedosahli bychom kratsi delky)
                while (count--) {
                    if(ch == RLE_FLAG){ //vstupni hodnotu 0xFF zakodujeme jako dvojici hodnot 0xFF
                        (*encoded)[position++] = RLE_FLAG;
                        (*encoded)[position++] = RLE_FLAG;
                    } else {
                        (*encoded)[position++] = ch; //znaky presuneme na vystup
                    }
                    //printf("rle: %c\n", ch);
                }
            }
            else {
                while (count) {
                    (*encoded)[position++] = RLE_FLAG; //flag indikujici zakodovani

                    //pokud je pocet vetsi nez UINT8_MAX, zakodujeme nekolikrat
                    if ( count >= (UINT8_MAX - 1) ) {
                        (*encoded)[position++] = UINT8_MAX - 1; //pocet
                        (*encoded)[position++] = ch; //znak
                        count -= UINT8_MAX - 1;
                    } else {
                        (*encoded)[position++] = (uint8_t) count; //pocet
                        (*encoded)[position++] = ch; //znak
                        break;
                    }
                    //printf("rle: %c\n", ch);
                }
            }

            //pokud nejsme na konci bloku, zapamatujeme si aktualni znak
            if (i != length) {
                ch = block[i];
            }
            count = 1; //pocet resetujeme na 1
        } else {
            count++; //inkrementujeme vyskyt
        }
    }

    *newLength = (size_t) position;

    return E_OK;
}

/**
 * RLE decode
 * @param block vstupni data
 * @param length delka vstupnich dat
 * @param decoded dekodovana data
 * @param newLength delka dekodovanych dat
 * @return
 */
tErrorCode
RLEDecode(const uint8_t *block, size_t length, uint8_t **decoded, size_t *newLength)
{
    *decoded = (uint8_t *) calloc (BLOCK_SIZE * 2, sizeof (uint8_t)); //viz kodovani, pouzijeme stejnou velikost
    if(*decoded == NULL) {
        return E_MALLOC;
    }

    size_t position = 0;
    uint8_t count;
    for (size_t i = 0; i < length; i++) {
        //na vstupu je RLE_FLAG (0xFF) ktery byl v puvodnich datech
        //je zakodovan jako dvojice po sobe jdoucich RLE_FLAG
        //na vystup dame jen jeden RLE_FLAG
        if( block[i] == RLE_FLAG && (i + 1) < length && block[i + 1] == RLE_FLAG) {
            (*decoded)[position++] = RLE_FLAG;
            i++;
            continue;
        }

        if ( block[i] == RLE_FLAG && (i + 2) < length) { //znak byl zakodovan
            count = block[++i]; //pocet vyskytu
            ++i; //index znaku
            while (count--) {
                (*decoded)[position++] = block[i]; //index vlozime na vystup dle poctu
            }
        }
        else {
            (*decoded)[position++] = block[i]; //znak nebyl zakodovan, jen prekopirujeme na vystup
        }
    }

    *newLength = (size_t) position; //delka dekodovaneho retezce

    return E_OK;
}

/**
 * SHC encode
 * @param block
 * @param length
 * @param encoded
 * @param newLength
 * @return
 */
tErrorCode
SHCEncode(const uint8_t *block, size_t length, uint8_t **encoded, size_t *newLength)
{
    tTree *tree;
    tErrorCode e;

    //vytvorime a inicializujeme strom
    if((e = initSHC(&tree)) != E_OK) {
        return e;
    }

    //vytvoreni listu s odpovidajicimi frekvencemi vyskytu danych ASCII hodnot
    for (size_t i = 0; i < length; ++i) {
        if (tree->leafs[block[i]] != NULL) {
            tNode *n = tree->leafs[block[i]];
            n->freq++;
        } else {
            createLeafSHC(&tree, block[i]);
        }
    }

    //nastaveni velikosti dle realneho poctu symbolu
    setRealSizeSHC(&tree);
    sortNodes(&tree);

    //printf("nextPosition: %d\n", (*tree).nextPosition);
    //printf("size: %d\n", (*tree).size);

    size_t offset = 0;
    while ( (*tree).nextPosition != (*tree).size ) {

        //spojovani uzlu s nejnizsi frekvenci vyskytu
        createParentNodeSHC(&tree, tree->nodes[0]->freq + tree->nodes[1]->freq, tree->nodes[0], tree->nodes[1]);

        sortNodes(&tree);
        offset++;
    }

    //koren
    tree->root = tree->nodes[0];

    size_t lengthHuffmanCode = 0;
    size_t symbolsCount = 0;
    //vygenerovani kodu jednotlivych uzlu
    for (size_t i = 0; i < MAX_LEAF_COUNT ; ++i) {
        if (tree->leafs[i] != NULL) {
            generateCodeForLeaf(tree->leafs[i]);
            lengthHuffmanCode += tree->leafs[i]->freq * tree->leafs[i]->codeLength;
            symbolsCount++;
        }
    }

    *newLength = ((size_t) ceil(lengthHuffmanCode / 8.0));
    *newLength += symbolsCount  * 3;
    *newLength += 3; //oddelovac

    *encoded = (uint8_t *) calloc (*newLength, sizeof (uint8_t));
    if(*encoded == NULL) {
        return E_MALLOC;
    }

    size_t pos = 0;
    //zakodovani informaci o listech stromu
    for (size_t i = 0; i < MAX_LEAF_COUNT ; ++i) {
        if (tree->leafs[i] != NULL) {
            (*encoded)[pos++] = tree->leafs[i]->value;

            //ulozeni indexu za zakodovane data
            #if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
                //MSB on left
                (*encoded)[pos++] = (tree->leafs[i]->freq >> 8) & 0xFF;
                (*encoded)[pos++] = tree->leafs[i]->freq & 0xFF;
            #else
                //MSB on right
                encoded[pos++] = tree->leafs[i]->freq & 0xFF;
                encoded[pos++] = (tree->leafs[i]->freq >> 8) & 0xFF;
            #endif
        }
    }

    //oddelovac
    (*encoded)[pos++] = 0;
    (*encoded)[pos++] = 0;
    (*encoded)[pos++] = 0;

    int8_t shift = 7;
    //zkodovani vstupnich dat
    for (size_t i = 0; i < length; ++i) {
        for (uint8_t j = 0; j < tree->leafs[ block[i] ]->codeLength; j++, shift--) {
            if (shift == -1) {
                shift = 7;
                pos++;
            }

            (*encoded)[pos] |= (tree->leafs[ block[i] ]->code[j] - '0') << shift;
        }

        //printf("shc: %d\n", block[i]);
    }

    //printf("bits: %d\n", bits);
    //printf("bytes: %d\n", b+1);
    //printf("znaku: %d\n", length);


    //printTree(&tree);
    disposeSHC(&tree);

    return E_OK;
}

/**
 * SHC decode
 * @param bwted
 * @param input
 * @param decoded
 * @param newLength
 * @return
 */
tErrorCode
SHCDecode(tBWTED *bwted, FILE *input, uint8_t **decoded, size_t *newLength)
{
    tTree *tree;
    tErrorCode e;

    //vytvorime a inicializujeme strom
    if((e = initSHC(&tree)) != E_OK) {
        return e;
    }
    *newLength = 0;
    size_t length = 0, read;
    uint8_t buf[3] = {0};

    //cteme metadata (udaje o listech stromu) dokud nedorazime na sekvenci 000
    //za timto jiz mame samotna zakodovana data
    while((read = fread(buf, sizeof (uint8_t), 3, input)) > 0) {
        //pocitame delku zakodovanych dat
        bwted->codedSize += read;

        if( buf[0] == 0 && buf[1] == 0 && buf[2] == 0) {
            break;
        }

        length += read; //pocitame, kolik jsme precetli celkem znaku

        //znak
        uint8_t *c = &buf[0];

        //extrakce indexu
        uint16_t freq = (buf[1] << 8) + buf[2];

        //vytvoreni listu s odpovidajicimi hodnotou a cetnosti vyskytu
        tNode *node = createLeafSHC(&tree, *c);
        node->freq = freq;

        size_t tmpLength = *newLength;
        *newLength += freq;

        if (tmpLength > *newLength) {
            //preteklo => nevalidni vstupni soubor
            disposeSHC(&tree);
            return E_NOT_VALID_INPUT_FILE;
        }

        //nulovani za pomoci XORu
        buf[0] ^= buf[0];
        buf[1] ^= buf[1];
        buf[2] ^= buf[2];
    }

    if( *newLength == 0 ) {
        disposeSHC(&tree);
        return E_OK;
    }

    //nastaveni velikosti dle realneho poctu symbolu
    setRealSizeSHC(&tree);
    sortNodes(&tree);

    //sestaveni stromu
    size_t offset = 0;
    while ( (*tree).nextPosition != (*tree).size ) {
        //spojovani uzlu s nejnizsi frekvenci vyskytu
        createParentNodeSHC(&tree, tree->nodes[0]->freq + tree->nodes[1]->freq, tree->nodes[0], tree->nodes[1]);

        sortNodes(&tree);
        offset++;
    }

    //koren
    tree->root = tree->nodes[0];


    size_t lengthOfHuffmanCode = 0;
    size_t symbolsCount = 0;
    //vygenerovani kodu jednotlivych uzlu
    for (size_t i = 0; i < MAX_LEAF_COUNT ; ++i) {
        if (tree->leafs[i] != NULL) {
            generateCodeForLeaf(tree->leafs[i]);
            lengthOfHuffmanCode += tree->leafs[i]->freq * tree->leafs[i]->codeLength;
            symbolsCount++;
        }
    }

    //printf("lengthOfHuffmanCode: %d\n", lengthOfHuffmanCode);

    //pocet bytu dat
    size_t bytesOfData = ((size_t) ceil(lengthOfHuffmanCode / 8.0));

    //pocitame delku zakodovanych dat
    bwted->codedSize += bytesOfData;

    //pole bytu dat
    uint8_t *block = (uint8_t *) calloc (bytesOfData, sizeof (uint8_t));
    if(block == NULL) {
        return E_MALLOC;
    }

    //pole bytu naplnime daty
    if (fread (block, sizeof (uint8_t), bytesOfData, input) <= 0)
    {
        free(block);
        disposeSHC(&tree);
        return E_OK;
    }

    //pole pro dekodovana data
    *decoded = (uint8_t *) calloc (*newLength, sizeof (uint8_t));
    if(*decoded == NULL) {
        return E_MALLOC;
    }

    //cteme jednotlive byty a v kazdem bytu jednotlive bity, kdy na zaklade hodnot bitu se pohubujeme ve strome
    //jakmile narazime na listovy uzel, dame na vystup dekodovanou hodnotu a cely proces zopakujeme
    size_t bits = 1;
    size_t pos = 0;
    tNode *tmpNode = (*tree).root;
    for (size_t i = 0; i < bytesOfData; ++i) {
        for (int8_t shift = 7; shift != -1 ; shift--) {
            if (bits > lengthOfHuffmanCode+1) {
                break;
            }

            if (tmpNode == NULL) {
                tmpNode = (*tree).root;
            }

            if( tmpNode->left == NULL && tmpNode->right == NULL ) {
                //printf("kod: %s\n", tmpNode->code);
                //printf("znak (%d): %c (%d)\n", pos, tmpNode->value, tmpNode->value);
                (*decoded)[pos++] = tmpNode->value;

                //zacneme znovu od korene
                tmpNode = (*tree).root;
            }

            if( ((block[i] >> shift) & 0x1) == LEFT ) { //vymaskujeme hodnotu bitu z aktualniho bytu
                tmpNode = tmpNode->left; //jdeme levym synem
            } else {
                tmpNode = tmpNode->right; //jdeme pravym synem
            }

            //printf("bit: %d s hodnotou %d\n", bits, ((block[i] >> shift) & 0x1));


            bits++;
        }

        //break v druhem cyklu
        if (bits > lengthOfHuffmanCode+1) {
            break;
        }
    }

    //cyklus skoncil, ale vypiseme jeste posledni znak, pokud pocet bitu neodpovida delce dat
    if (bits <= lengthOfHuffmanCode+1) {
        if( tmpNode->left == NULL && tmpNode->right == NULL ) {
            (*decoded)[pos++] = tmpNode->value;
        }
    }


    //printf("bits: %d\n", bits > lengthOfHuffmanCode ? bits-1 : bits);
    //printf("bytes: %d\n", bytesOfData);
    //printf("znaku: %d\n", *newLength);

    //printTree(&tree);
    disposeSHC(&tree);
    free(block);

    return E_OK;
}

/**
 * Kodovani
 * @param bwted
 * @param input
 * @param output
 * @return
 */
tErrorCode
BWTEncoding (tBWTED *bwted, FILE *input, FILE *output)
{
    uint8_t block[BLOCK_SIZE];
    size_t length; //delka nactenych dat
    size_t newLength; //nova delka, pomocna promenna
    tErrorCode e;

    while((length = fread(block, sizeof (uint8_t), BLOCK_SIZE, input)) > 0) {
        bwted->uncodedSize += length;

        // 1. BWT
        uint8_t *bwt = NULL;
        e = BWTEncode(block, length, &bwt, &newLength);
        if( e != E_OK ) {
            if( bwt != NULL ) {
                free(bwt);
            }
            return e;
        }

        // 2. MTF
        uint8_t *mtf = NULL;
        e = MTFEncode(bwt, newLength, &mtf, &newLength);
        free(bwt);
        if( e != E_OK ) {
            if( mtf != NULL ) {
                free(mtf);
            }
            return e;
        }

        // 3. RLE
        uint8_t *rle = NULL;
        e = RLEEncode(mtf, newLength, &rle, &newLength);
        free(mtf);
        if( e != E_OK ) {
            if( rle != NULL ) {
                free(rle);
            }
            return e;
        }

        // 4. SHC
        uint8_t *shc = NULL;
        e = SHCEncode(rle, newLength, &shc, &newLength);
        free(rle);
        if( e != E_OK ) {
            if( shc != NULL ) {
                free(shc);
            }
            return e;
        }

        bwted->codedSize += newLength;


        if(fwrite(shc, sizeof(uint8_t), newLength, output) <= 0) {
            return E_WRITE;
        }
        free(shc);
        memset (block, 0, BLOCK_SIZE);
    }

    return E_OK;
}

/**
 * Dekodovani
 * @param bwted
 * @param input
 * @param output
 * @return
 */
tErrorCode
BWTDecoding (tBWTED *bwted, FILE *input, FILE *output)
{
    size_t newLength;
    tErrorCode e;

    while(1) {
        newLength = 0;
        // 1. SHC
        uint8_t *shc = NULL;
        e = SHCDecode(bwted, input, &shc, &newLength);
        if( e != E_OK ) {
            if( shc != NULL ) {
                free(shc);
            }
            return e;
        }

        if( newLength == 0 ) {
            free(shc);
            break;
        }

        // 2. RLE
        uint8_t *rle = NULL;
        e = RLEDecode(shc, newLength, &rle, &newLength);
        free(shc);
        if( e != E_OK ) {
            if( rle != NULL ) {
                free(rle);
            }
            return e;
        }

        // 3. MTF
        uint8_t *mtf = NULL;
        e = MTFDecode(rle, newLength, &mtf, &newLength);
        free(rle);
        if( e != E_OK ) {
            if( mtf != NULL ) {
                free(mtf);
            }
            return e;
        }

        // 4. BWT
        uint8_t *bwt = NULL;
        e = BWTDecode(mtf, newLength, &bwt, &newLength);
        free(mtf);
        if( e != E_OK ) {
            if( bwt != NULL ) {
                free(bwt);
            }
            return e;
        }

        bwted->uncodedSize += newLength;

        if(fwrite(bwt, sizeof(uint8_t), newLength, output) <= 0){
            return E_WRITE;
        }
        free(bwt);
    }

    return E_OK;
}