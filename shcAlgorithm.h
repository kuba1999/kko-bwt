/**
 * @author Jakub Svestka - xsvest05
 * @date 17. 2. 2017
 * shcAlgorithm.c
 *
 * Pomocne datove typy
 */

#ifndef SHC_ALGORITHM_H
#define SHC_ALGORITHM_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"

#define MAX_LEAF_COUNT  (UINT8_MAX + 1)
//pocet uzlu v plnem binarnim strome dle vzorce: 2 * n_0 - 1
#define MAX_NODE_COUNT (2 * MAX_LEAF_COUNT - 1)

//typ syna
typedef enum {
    RIGHT = 0,
	LEFT = 1,
    NONE
} tSonType;

//uzel ve strome
typedef struct sNode {
    uint8_t value; //ASCII hodnota
	size_t freq; //pocet vyskytu
    tSonType son; //typ syna
    char code[16]; //huffmanuv kod
    uint8_t codeLength; //delka huffmanova kodu
	struct sNode *left; //levy syn
	struct sNode *right; //pravy syn
	struct sNode *parent; //rodic
} tNode;

//struktura celeho stromu
typedef struct {
    size_t size; //pocet uzlu
    size_t nextPosition; //dalsi pozice kam ulozit novy uzel
    tNode **nodes; //uzly
    tNode **leafs; //listy, ktere jsou ukladany na pozice dle ASCII hodnoty
    tNode *root; //koren
} tTree;

tErrorCode initSHC(tTree **);
void disposeSHC(tTree **);
tNode* createLeafSHC(tTree **, uint8_t);
tNode* insertNodeSHC(tTree **, uint8_t, size_t, struct sNode *, struct sNode *, struct sNode *);
tNode* createParentNodeSHC(tTree **, size_t, struct sNode *, struct sNode *);
void printTree(tTree **);
void sortNodes(tTree **);
void setRealSizeSHC(tTree **);
void generateCodesSHC(tTree **);
void generateCodeForLeaf(tNode *);


#endif